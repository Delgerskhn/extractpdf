using System.Collections.Generic;
using System.IO;
using extractpdf.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.xtra.iTextSharp.text.pdf.pdfcleanup;

namespace extractpdf.PdfUtils
{
    public static class PdfCropper
    {
        public const int pageNumber = 1;
        public static Rectangle generateCropArea(Rectangle pageSize, PdfInfo fileInfo, PdfInfo cropSizePercentActsPoint)
        {
            var llx = pageSize.Width * fileInfo.llXPercent;
            var lly = pageSize.Height * (1 - fileInfo.llYPercent);
            var urx = pageSize.Width * fileInfo.urXPercent;
            var ury = pageSize.Height * (1 - fileInfo.urYPercent);
            cropSizePercentActsPoint.llXPercent = llx;
            cropSizePercentActsPoint.llYPercent = lly;
            cropSizePercentActsPoint.urXPercent = urx;
            cropSizePercentActsPoint.urYPercent = ury;
            return new iTextSharp.text.Rectangle(llx, lly, urx, ury);
        }
        public static void inverseCroppedAreaForCleanup(PdfInfo cropSizePercentActsPoint, List<PdfCleanUpLocation> cleanUpLocations, Rectangle pageSize)
        {
            Rectangle left = new Rectangle(0, 0, (int)cropSizePercentActsPoint.llXPercent, pageSize.Height);
            Rectangle top = new Rectangle(cropSizePercentActsPoint.llXPercent, cropSizePercentActsPoint.urYPercent, pageSize.Width, pageSize.Height);
            Rectangle right = new Rectangle(cropSizePercentActsPoint.urXPercent, 0, pageSize.Width, cropSizePercentActsPoint.urYPercent);
            Rectangle bottom = new Rectangle(cropSizePercentActsPoint.llXPercent, 0, cropSizePercentActsPoint.urXPercent, cropSizePercentActsPoint.llYPercent);

            cleanUpLocations.Add(new PdfCleanUpLocation(1, left, iTextSharp.text.BaseColor.WHITE));
            cleanUpLocations.Add(new PdfCleanUpLocation(1, top, iTextSharp.text.BaseColor.WHITE));
            cleanUpLocations.Add(new PdfCleanUpLocation(1, right, iTextSharp.text.BaseColor.WHITE));
            cleanUpLocations.Add(new PdfCleanUpLocation(1, bottom, iTextSharp.text.BaseColor.WHITE));
        }
        public static void CropDocument(PdfInfo fileInfo, string oldchar, string repChar, string pdfRootPath)
        {
            var filePath = System.IO.Path.Combine(pdfRootPath, fileInfo.FileName);
            PdfReader reader = new PdfReader(filePath);

            PdfStamper stamper = new PdfStamper(reader, new FileStream(filePath.Replace(oldchar, repChar), FileMode.Create, FileAccess.Write));
            List<PdfCleanUpLocation> cleanUpLocations = new List<PdfCleanUpLocation>();

            iTextSharp.text.Rectangle pageSize = reader.GetPageSize(pageNumber);
            PdfInfo cropSizePercentActsPoint = new PdfInfo();
            iTextSharp.text.Rectangle cropArea = generateCropArea(pageSize, fileInfo, cropSizePercentActsPoint);
            inverseCroppedAreaForCleanup(cropSizePercentActsPoint, cleanUpLocations, pageSize);
            PdfCleanUpProcessor cleaner = new PdfCleanUpProcessor(cleanUpLocations, stamper);
            cleaner.CleanUp();
            stamper.Close();

            // Document document = new Document(cropArea);
            // PdfWriter writer = PdfWriter.GetInstance(document,
            // new FileStream(filePath.Replace(oldchar, oldchar.Replace(".pdf", "_crop.pdf")),
            // FileMode.Create, FileAccess.Write));
            // document.Open();
            // PdfContentByte cb = writer.DirectContent;
            // document.NewPage();
            // PdfImportedPage page = writer.GetImportedPage(reader,
            // pageNumber);
            // cb.AddTemplate(page, 0, 0);
            // document.Close();
            reader.Close();
        }
    }
}