﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace extractpdf.Models
{
    public class PdfInfo
    {
        public string FileName { get; set; }
        public float llXPercent { get; set; }//lower left x
        public float llYPercent { get; set; }//lower left y
        public float urXPercent { get; set; }//upper right x
        public float urYPercent { get; set; }//upper right y
    }
}
