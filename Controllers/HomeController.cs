﻿using extractpdf.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using GroupDocs.Parser;
using extractpdf.PdfUtils;

namespace extractpdf.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly string pdfRootPath;
        private readonly string imgRootPath;
        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            _logger = logger;
            pdfRootPath = System.IO.Path.Combine(_hostEnvironment.WebRootPath, "pdfs");
            imgRootPath = System.IO.Path.Combine(_hostEnvironment.WebRootPath, "imgs");
        }

        public IActionResult Index()
        {
            List<Models.PdfInfo> fileInfos = new List<Models.PdfInfo>();
            string[] fileEntries = Directory.GetFiles(pdfRootPath);
            foreach (string fileName in fileEntries)
            {
                var fname = System.IO.Path.GetFileName(fileName);
                PdfInfo info = new PdfInfo { FileName = fname };
                fileInfos.Add(info);
            }
            return View(fileInfos);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Extract(string fileName)
        {
            ViewBag.imgPath = fileName + ".png";
            ViewBag.pdfPath = fileName;
            return View();
        }

        [HttpPost]
        public IActionResult ExtractPdf([FromBody] PdfInfo info)
        {
            try
            {
                string cropName = info.FileName.Replace(".pdf", "_crop.pdf");
                PdfCropper.CropDocument(info, info.FileName, cropName, pdfRootPath);
                var cropPdfPath = System.IO.Path.Combine(pdfRootPath, cropName);
                List<string> imgs = ExtractImage(cropPdfPath, cropName);
                List<string> paragraphs = ExtractText(cropPdfPath);
                return Ok(new
                {
                    Status = 1,
                    imgs = imgs,
                    paragraphs = paragraphs
                });
            }
            catch (Exception e)
            {
                return Ok(new
                {
                    Status = 0,
                    Message = e.Message
                });
            }


        }

        public List<string> ExtractText(string pdfPath)
        {
            using (Parser parser = new Parser(pdfPath))
            {
                using (TextReader reader = parser.GetText())
                {
                    string result = reader.ReadToEnd();
                    string[] paragraphs = result.Split("\r\n\r\n");
                    return paragraphs.ToList();
                }
            }
        }

        public void SavePdfAsImg(string fileName)
        {
            var filePath = System.IO.Path.Combine(pdfRootPath, fileName);
            Spire.Pdf.PdfDocument pdfdocument = new Spire.Pdf.PdfDocument(filePath);
            System.Drawing.Image image = pdfdocument.SaveAsImage(0);
            image.Save(System.IO.Path.Combine(imgRootPath, fileName + ".png"), System.Drawing.Imaging.ImageFormat.Png);
        }

        [HttpPost]
        public async Task<IActionResult> Create(IFormFile file, Models.PdfInfo fileInfo)
        {
            var fileName = System.IO.Path.GetFileName(file.FileName);
            var filePath = System.IO.Path.Combine(pdfRootPath, fileName);
            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            SavePdfAsImg(fileName);
            return RedirectToAction(nameof(Extract), new { fileName = fileName });
        }
        [HttpGet]
        public ActionResult GetPdf(string fileName)
        {
            string filePath = "~/file/" + fileName;
            Response.Headers.Add("Content-Disposition", "inline; filename=" + fileName);
            return File(filePath, "application/pdf");
        }

        public List<string> ExtractImage(string physicalPath, string fileName)
        {
            // Create an instance of Parser class
            //var path = _hostEnvironment.WebRootFileProvider.GetFileInfo("input.pdf")?.PhysicalPath;
            List<string> imgs = new List<string>();
            Dictionary<string, System.Drawing.Image> images = PdfImageExtractor.ExtractImages(physicalPath, 1);
            int counter = 1;
            foreach (KeyValuePair<string, System.Drawing.Image> pair in images)
            {
                string imgName = string.Format("{0}_img_{1}.Jpeg", fileName, counter++);
                string imgPath = System.IO.Path.Combine(imgRootPath, imgName);
                pair.Value.Save(imgPath, ImageFormat.Png);
                imgs.Add(imgName);
            }
            return imgs;
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
