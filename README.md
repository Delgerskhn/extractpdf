## ExtractPDF project
PDF өргөтгөлтэй файлаас зураг болон текст гарган авах зориулалт бүхий вэбсайт. 

**Demo website:** https://extractpdf.herokuapp.com/

## Сайтын логик
Controllers/HomeController.cs файл дотор програмын үндсэн логик илэрхийллүүдийг бичсэн болно. 

## Сайтыг ашиглах
1. Нүүр хуудас дээрх **Create New** товчийг дарж боловсруулахыг хүссэн PDF файлыг сонгоод **Create** товчийг дарна. 

2. Дээрх алхамыг гүйцэтгэсний дараа PDF файл тохиргоо хийх хуудасруу шилжих бөгөөд PDF файлыг харуулах хэсэг дээр Mouse ашиглан боловсруулалт хийх талбарыг тохируулж сонгож өгнө. 

3. PDF файлын доор байрлах **Extract information** товч дээр дарж хэсэг хугацааны дараа боловсруулагдсан зураг болон текстэн мэдээлэл Textarea талбарт харагдах болно. 

## Ашигласан технологи
1. ASP.NET Core 5.0 MVC framework
2. Docker container
3. Heroku devops

## Ашигласан сан
1. [ITextSharp](https://www.nuget.org/packages/iTextSharp/) 
2. [GroupDocs](https://www.nuget.org/packages/GroupDocs.Parser/)

> Written with [StackEdit](https://stackedit.io/).
