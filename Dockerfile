
#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app

RUN apt-get update
RUN apt-get install -y libgdiplus

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["extractpdf.csproj", "extractpdf/"]
RUN dotnet restore "extractpdf/extractpdf.csproj"
WORKDIR "/src/extractpdf"
COPY . .
RUN dotnet build "extractpdf.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "extractpdf.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "extractpdf.dll"]
CMD ASPNETCORE_URLS=http://*:$PORT dotnet extractpdf.dll